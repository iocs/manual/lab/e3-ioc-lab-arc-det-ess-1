require xtpico
require essioc

iocshLoad("$(essioc_DIR)common_config.iocsh")

epicsEnvSet("PREFIX",           "$(PREFIX=LAB-010:RFS-ADS1)")
epicsEnvSet("DEVICE_IP",        "rf-arcdet.cslab.esss.lu.se")

epicsEnvSet("I2C_COMM_PORT",    "AK_I2C_COMM")
epicsEnvSet("R_TMP100",         ":Temp")
epicsEnvSet("R_AD5144A_1",      ":Res1")
epicsEnvSet("R_AD5144A_2",      ":Res2")
epicsEnvSet("R_AD5144A_3",      ":Res3")
epicsEnvSet("R_AD5144A_4",      ":Pot")
epicsEnvSet("R_TCA9555",        ":IOExp")
epicsEnvSet("R_LTC2991",        ":VMon")
epicsEnvSet("R_M24M02",         ":Eeprom")


drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):1003")

# Temperature sensor
iocshLoad("$(xtpico_DIR)/tmp100.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=$(R_TMP100), COUNT=1, INFOS=0x49")

# Pulse width threshold
iocshLoad("$(xtpico_DIR)/ad5144a.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, RDAC=1, NAME=$(R_AD5144A_1), COUNT=1, INFOS=0x2B")

# Comparator level threshold
iocshLoad("$(xtpico_DIR)/ad5144a.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, RDAC=2, NAME=$(R_AD5144A_2), COUNT=1, INFOS=0x2B")

# PMT Gain
iocshLoad("$(xtpico_DIR)/ad5144a.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, RDAC=3, NAME=$(R_AD5144A_3), COUNT=1, INFOS=0x2B")

# Test LED current level
iocshLoad("$(xtpico_DIR)/ad5144a.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, RDAC=4, NAME=$(R_AD5144A_4), COUNT=1, INFOS=0x2B")

# I2C port extender
iocshLoad("$(xtpico_DIR)/tca9555.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=$(R_TCA9555), COUNT=1, INFOS=0x21")

# Voltage and current monitor
iocshLoad("$(xtpico_DIR)/ltc2991.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=$(R_LTC2991), COUNT=1, INFOS=0x48")

# EEPROM
iocshLoad("$(xtpico_DIR)/m24m02.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=$(R_M24M02), COUNT=1, INFOS=0x50")

# Arc detector alias Pvs
dbLoadRecords("$(E3_CMD_TOP)/arc_det.db", "P=$(PREFIX), R_LTC2991=$(R_LTC2991)-, R_TMP100=$(R_TMP100)-, R_AD5144A_1=$(R_AD5144A_1)-, R_AD5144A_2=$(R_AD5144A_2)-, R_AD5144A_3=$(R_AD5144A_3)-, R_AD5144A_4=$(R_AD5144A_4)-, R_TCA9555=$(R_TCA9555)-, R_M24M02=$(R_M24M02)-")


pvlistFromInfo("ARCHIVE_THIS", "$(IOCNAME):ArchiverList")

## Port Expander Initial config (set ports 11-15 to output and initialize then as 0)
afterInit(dbpf("$(PREFIX)$(R_TCA9555)-DirPin11", 0))
afterInit(dbpf("$(PREFIX)$(R_TCA9555)-DirPin12", 0))
afterInit(dbpf("$(PREFIX)$(R_TCA9555)-DirPin13", 0))
afterInit(dbpf("$(PREFIX)$(R_TCA9555)-DirPin14", 0))
afterInit(dbpf("$(PREFIX)$(R_TCA9555)-DirPin15", 0))
afterInit(dbpf("$(PREFIX)$(R_TCA9555)-LevelPin11", 0))
afterInit(dbpf("$(PREFIX)$(R_TCA9555)-LevelPin12", 0))
afterInit(dbpf("$(PREFIX)$(R_TCA9555)-LevelPin13", 0))
afterInit(dbpf("$(PREFIX)$(R_TCA9555)-LevelPin14", 0))
afterInit(dbpf("$(PREFIX)$(R_TCA9555)-LevelPin15", 0))

iocInit()
