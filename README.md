# e3-ioc-rfq-arc-det-2

IOC repository for RF station xtpico (ESS) arc detector number 1 in RFQ.

**PREFIX**: `LAB-010:RFS-ADS1`

**IOC-name**: `LabS-ICS:SC-IOC-272`
